<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportCounties extends Command
{
    protected $signature = 'app:import-counties {filename} {database_name?}';
    protected $description = 'CSV-ből importál az adatbázisba.';

    public function handle()
    {
        $filename = $this->argument('filename');
        $databaseName = $this->argument('database_name') ?? config('database.default');

        $csvData = $this->getCsvData($filename);

        if (!$csvData) {
            return;
        }

        foreach ($csvData as $row) {
            $this->importCounty($row, $databaseName);
        }

        $this->info('Counties imported successfully!');
    }

    private function getCsvData($filename, $withHeader = true)
    {
        if (!file_exists($filename)) {
            echo "$filename nem található";
            return false;
        }

        $csvfile = fopen($filename, 'r');
        $header = fgetcsv($csvfile);

        if (!$withHeader) {
            $lines = [];
        } else {
            $lines[] = $header;
        }

        while (!feof($csvfile)) {
            $line = fgetcsv($csvfile);
            $lines[] = $line;
        }

        fclose($csvfile);
        return $lines;
    }

    private function importCounty($row, $databaseName)
{
    // Check if $row is a valid array
    if (is_array($row)) {
        // Assuming the first column of your CSV is the county name
        $countyName = $row[0];

        // Check if the county name is not empty before proceeding
        if (!empty($countyName)) {
            // Check if the county already exists in the database
            $existingCounty = DB::connection($databaseName)
                ->table('varmegye')
                ->where('name', $countyName)
                ->first();

            if (!$existingCounty) {
                // If it doesn't exist, insert it into the database
                DB::connection($databaseName)->table('varmegye')->insert(['name' => $countyName]);
                $this->info('County added: ' . $countyName);
            } else {
                // If it already exists, log it
                $this->info('County already exists: ' . $countyName);
            }
        } else {
            // Log a warning if the county name is empty
            $this->warn('Empty county name encountered.');
        }
    } else {
        // Log a warning if $row is not a valid array
        $this->warn('Invalid row encountered.');
    }
}
}
