<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class ImportCounties extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:import-counties {filename} {database_name?}';

    protected $description = 'CSV-ből importál az adatbázisba.';


    /**
     * Execute the console command.
     */
    public function handle()
    {
        $filename = $this->argument('filename');
        $databaseName = $this->argument('database_name') ?? config('database.default'); 

 
        $importer = new CountiesImport($filename, $databaseName);
        $importer->import();

        $this->info('Counties imported successfully!');
    }

    private function getCsvData($filename, $withHeader = true)
    {
        if(!file_exists($fileName)){
        echo "$filename nem található";
        return false;
        }

        $csvfile = fopen($fileName, 'r');
        $header = fgetcsv($csvfile);
        if($withHeader){
            $lines[] = $header;
        }
        else{
            $lines = [];
        }
        while(!feof($csvfile)){
            $line = fgetcsv($csvfile);
            $lines[] = $line;
        }
        fclose($csvfile);
        return $lines;
    }

    

}
class CountiesImport implements ToModel
{
    private $filename;
    private $databaseName;

    public function __construct($filename, $databaseName)
    {
        $this->filename = $filename;
        $this->databaseName = $databaseName;
    }

    public function model(array $row)
{
    // Ellenőrizd, hogy a megye már létezik az adatbázisban
    $existingCounty = DB::connection($this->databaseName)
        ->table('varmegye')
        ->where('name', $row['name'])
        ->first();

    // Ha nem létezik, akkor hozzáadhatod az adatbázishoz
    if (!$existingCounty) {
        DB::connection($this->databaseName)->table('varmegye')->insert($row);
        $this->info('County added: ' . $row['name']);
        
        // Visszaadhatod a modellt is
        return new County([
            'name' => $row['name'],
            // További attribútumokat is hozzáadhatsz, ha szükséges
        ]);
    } else {
        // Ha már létezik, akkor csak naplózd és ne add hozzá újra
        $this->info('County already exists: ' . $row['name']);
        return null; // Ne adj vissza új modellt, mivel már létezik
    }
}

public function import()
{
    Excel::filter('chunk')->load($this->filename)->chunk(250, function ($results) {
        foreach ($results->toArray() as $row) {
            $this->model($row);
        }
    });
}
}
