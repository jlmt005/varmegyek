<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <title>{{ config('app.name', 'Vármegyék') }}</title>
    <link rel="icon" type="image/x-icon" href="/img/favicon.ico">
    <link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="{{ asset('build/assets/app-c75e0372.js')}}"></script>
    <script src="{{ asset('js/varmegye.js') }}"></script>

    <link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet" type="text/css">
    
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link href="{{ asset('build/assets/app-041e359a.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('css/varmegye.css') }}" type="text/css" rel="stylesheet">

    <style type="text/css">
        body {
            padding-top: 70px; /* Adjust the top padding for the fixed navbar */
        }

        .navbar-brand i {
            font-size: 20px;
        }

        .navbar-toggler-icon {
            background-color: #827471;
        }

        .navbar-nav a {
            margin-right: 10px;
            color: #007bff;
        }

        .navbar-nav a:hover {
            color: #0056b3;
        }

        footer {
            margin-top: 10px;
            padding: 50px;
            background-color: #423735;
        }
    </style>
</head>
<body>
    <div id="app">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light bg-white fixed-top shadow-sm">
                <div class="container-fluid">
                    <a class="navbar-brand" href="{{ url('/') }}"><i class="fa fa-home"></i></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li><a class="nav-link" href="{{route('varmegyek')}}">Vármegyék</a></li>
                        </ul>

                        <ul class="navbar-nav ms-auto">

                        </ul>
                    </div>
                </div>
            </nav>
        </div>

        <main class="py-4">
            @yield('content')
        </main>

        <footer class="text-center">
            {{ config('app.name', 'Vármegyék') }} v{{ env('APP_VERSION') }} (PHP v{{ PHP_VERSION }})
        </footer>
    </div>
</body>
</html>
