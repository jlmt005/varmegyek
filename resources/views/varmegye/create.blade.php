@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <form method="post" action="{{ route('saveVarmegye') }}" accept-charset="UTF-8">
                    @csrf
                    <div class="card-header bg-info text-white">
                        {{ __('Új vármegye') }}
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="name"><strong>Név</strong></label>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                    </div>
                    <div class="card-footer bg-light text-center">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i>&nbsp;{{__('Mentés')}}
                        </button>
                        <a class="btn btn-secondary" href="{{ route('varmegyek') }}">
                            <i class="fa fa-ban"></i>&nbsp;{{__('Mégse')}}
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
