<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Vármegyék') }}</title>
    <link rel="icon" type="image/x-icon" href="/img/favicon.ico">
    <link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet" type="text/css">
    <style>
        body {
            font-family: "Garamond";
            margin: 0;
            padding: 0;
            background-color: #DEE4E0;
            
        }

        header {
            background-color: #343a40;
            color: #fff;
            padding: 10px 20px;
            text-align: center;
        }

        .container {
            width: 50%;
            margin: 20px auto;
        }

        form {
            display: flex;
            margin-bottom: 20px;
        }

        form input {
            flex: 1;
            padding: 8px;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        form button {
            padding: 8px 15px;
            background-color: #007bff;
            color: #fff;
            border: none;
            border-radius: 4px;
 
            cursor: pointer;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 5px;
        }

        th, td, tr {
            padding: 2px;
            text-align: center;
            border-bottom: 1px solid #dee2e6;
            color: #fff;

        }
        td
        {
            width: 60px;    
            background: gray;
        }

        th {
            background-color: #DAE7DD;
            color: #000;
        }

        tr:hover {
            background-color: #f5f5f5;
        }

        .btn {
            padding: 8px 15px;
            margin-right:0 auto;
            background-color: #0F1310;
            color: #fff;
            border: none;

            border-radius: 4px;
            cursor: pointer;
            a
        }

        .btn-danger {
            background-color: #dc3545;
        }
    </style>
</head>
<body>

<header>
    <h4>Vármegye</h4>
</header>

<div class="container">
<form method="post" action="{{ route('searchVarmegyek') }}">
        @csrf
        <div class="input-group">
            <div class="input-group-prepend">
                <a class="btn btn-outline-secondary" href="{{ url('/') }}">
                    <i class="fas fa-home"></i>
                </a>
            </div>
            <input type="text" name="needle" class="form-control" placeholder="Keresés">
            <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </div>
        </div>
    </form>


    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <<table>
        <thead>
            <tr>
                <th>#</th>
                <th>Megnevezés</th>
                <th>Művelet</th>
            </tr>
        </thead>
        <tbody>
            @forelse($entities as $entity)
                <tr>
                    <td>{{ $entity->id }}</td>
                    <td>{{ $entity->name }}</td>
                    <td>
                        <form method="post" action="{{ route('editVarmegye', $entity->id) }}">
                            @csrf
                            <button class="btn btn-primary" type="submit">
                                <i class="fas fa-edit"></i> Módosít
                            </button>
                        </form>
                        <form method="post" action="{{ route('deleteVarmegye', $entity->id) }}">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger" type="submit">
                                <i class="fas fa-trash"></i> Töröl
                            </button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" style="text-align: center;">Nincs találat.</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>

</body>
</html>
